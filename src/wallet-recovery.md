# Derivation Paths

Wallets use the two following derivation paths:

1. `Account Private Key` = `m/0/0`
2. `Wallet Private Key` = `m/44'/0'/0'/0` 

### Account Private Key

A single private key used for the majority of actions like signing in, signing your Twetch data and ECIES key exchanges in encrypted chat. The corresponding public key is used when depositing funds into your account via address/qr code

Note: When importing into 3rd party wallet softwares, you may need to only input the derivation path as `m/0` or even just `m`. 

### Wallet Private Key

A rotating extended private key used to create new private keys for each transaction you make. Each time you receive funds on Twetch or via paymail, the corresponding public key is used to create a new address
