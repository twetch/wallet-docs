# Summary

# Twetch Wallet

- [Wallet Development](./wallet-dev/home.md)
  - [Detecting the provider](./wallet-dev/detect-provider.md)
  - [Establishing a connection](./wallet-dev/establish-connection.md)
  - [Displaying your app](./wallet-dev/display-app.md)
  - [Making Payments](./wallet-dev/making-payments.md)
  - [Sign Messages](./wallet-dev/sign-messages.md)

- [Derivation Paths](./wallet-derivation.md)
