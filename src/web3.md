[Documentation](README.md) / Exports

# Documentation

## Table of contents

### Variables

- [WalletProvider](modules.md#walletprovider)

### Functions

- [useProviderContext](modules.md#useprovidercontext)
- [useWalletProvider](modules.md#usewalletprovider)

## Variables

### WalletProvider

• `Const` **WalletProvider**: `Context`<{ `paymail`: `string` = ''; `publicKey`: `string` = ''; `setContext`: () => `void`  }\>

#### Defined in

useWalletProvider.ts:3

## Functions

### useProviderContext

▸ **useProviderContext**(): `Object`

#### Returns

`Object`

| Name | Type |
| :------ | :------ |
| `paymail` | `string` |
| `publicKey` | `string` |
| `setContext` | (`obj`: `WalletContext`) => `void` |

___

### useWalletProvider

▸ **useWalletProvider**(): `Object`

#### Returns

`Object`

| Name | Type |
| :------ | :------ |
| `paymail` | `string` |
| `publicKey` | `string` |
| `setContext` | () => `void` |

