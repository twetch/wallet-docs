The Twetch Wallet browser extension will inject an object called `bitcoin` on the [window](https://developer.mozilla.org/en-US/docs/Web/API/Window) object of any web application the user visits. The `bitcoin` object is also available on `window.twetch` to prevent namespace collisions.

To detect if a browser extension using this API is installed, you can check for the existence of the `bitcoin` object.

To make it easy to detect Twetch Wallet specifically, the extension adds an additional `isTwetch` flag.

```javascript
const isTwetchInstalled = window.bitcoin && window.bitcoin.isTwetch
```

If Twetch Wallet is not installed, we recommend you redirect your users to [our website](https://twetch.com). Altogether, this may look like the following.

```javascript
const getProvider = () => {
  if ("bitcoin" in window) {
    const provider = window.bitcoin;
    if (provider.isTwetch) {
      return provider;
    }
  }
  window.open("https://twetch.com/downloads", "_blank");
};
```
