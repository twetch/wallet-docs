<figure>
<img src="https://addons.mozilla.org/user-media/previews/full/264/264054.png" alt="Twetch Wallet Browser Extension ScreenShot" style="width:100%">
<figcaption align = "center"><b>Connection and transaction approval dialog for rarecandy.io</b></figcaption>
</figure>

When you are [Establishing a Connection](./establish-connection.md), or [Sending a Transaction](sending-a-transaction) to a user with Twetch Wallet, they are presented with the dialogs above. Twetch Wallet will inspect the HTML of the application's page itself in order to display the title and icon for the dialogs.

| **Value** | **Primary Lookup** | **Secondary Lookup**   | **Fallback**   |
|-----------|--------------------|------------------------|----------------|
| Title     | Open Graph Title   | Document Title Element | None           |
| Icon      | Apple Touch Icon   | Favicon                | A default icon |
