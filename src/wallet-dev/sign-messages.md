To sign messages using [BSM](https://github.com/bitcoin/bitcoin/pull/524) call the `sign-message` contract. It will look a little something like this:

```javascript
const response = await window.twetch.abi({
  contract: 'sign-message',
  method: 'sign-message',
  payload: {
    message: 'good morning'
  }
});
```

Response

```json
{
  "action": "APPROVE_ABI",
  "actionId": "dde23e3f-0baa-4148-8460-6060f5de7248",
  "address": "12tDncQvFZaZzqanupmtXpDUm42Wd4Cn4W",
  "message": "good morning",
  "origin": "https://wallet-docs.twetch.app",
  "portId": "b0fa5bc9-572a-4626-affd-97b16f8aa354",
  "sig": "Hz2pGEN3rI0mPevs3o5rseqZcefbOMwb6MECUj5KJ1TQVKQATiACKdrj29aGfq+1ofNcjvtWZpyKS0iO2Erg7r4="
}
```
