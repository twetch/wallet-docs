**Twetch Wallet** is a wallet and browser extension that can be used to manage digital assets and access decentralized applications on the [Bitcoin SV](https://bitcoinsv.com) blockchain.
 
It works by creating and managing private keys on behalf of its users, allowing them to store funds and sign transactions.
 
The extension injects a `bitcoin` object into the javascript context of every web application the user visits. The application may then interact with the wallet, such as asking for permission to perform a transaction, through this injected object.

## Pages


- [Detecting the provider](./detect-provider.md)

- [Establishing a connection](./establish-connection.md)

- [Displaying your app](./display-app.md)

- [Making Payments](./making-payments.md)

- [Sign Messages](./sign-messages.md)
